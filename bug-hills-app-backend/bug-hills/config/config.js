module.exports = {
    'secret': 'customer-super-secret-key',
    url: 'mongodb://localhost:27017/bug_hills_db',
    ROLEs: ['USER', 'ADMIN']
 };