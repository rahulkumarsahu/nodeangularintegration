const mongoose = require('mongoose'), Schema = mongoose.Schema;

const ModuleNameSchema = mongoose.Schema({
    moduleName: String,
});

module.exports = mongoose.model('Module', ModuleNameSchema);