const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const IssueDetailsSchema = mongoose.Schema({
    title: String,
    status: String,
    defectSummary: String,
    defectDescription: String,
    actionTaken: String,
    referId: String,
    defectId: String,
    moduleName: String,
    verifiedBy: String,
    ownerId: String,
    assignedTo: String,
    priority: String,
    severity: String,
    verificationStatus: String,
    defectInjected: String,
    defectCorrected: String,
    defectDetected: String,
    solutionComment: String,
    defectType: String,
    department: String,
    requester: String,
    plannedEffort: String,
    assignedDate: Date,
    expectedCloseDate: Date,
    actualEffort: String,
    actualFixedDate: Date,
    testCaseId: String,
    defectPhaseId: String,
    resolutionTime: Date,
    responseTime: Date,
    slaResolutionTime: Date,
    slaResponseTime: Date

});

module.exports = mongoose.model('Issue', IssueDetailsSchema);