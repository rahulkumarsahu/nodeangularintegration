const config = require('../config/config');
const csvjson = require('csvjson');
const Parser = require('json2csv');

const Role = require('../models/role.model');
const User = require('../models/user.model');
const ModuleName = require('../models/module.model');
const Issue = require('../models/issue.model');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.signup = (req, res) => {
    // Save User to Database
    console.log("Processing func -> SignUp");

    const user = new User({
        username: req.body.username,
        activation: "A",
        password: bcrypt.hashSync(req.body.password, 8)
    });

    // Save a User to the MongoDB
    user.save().then(savedUser => {
        Role.find({
            'name': {
                $in: req.body.roles.map(role => role.toUpperCase())
            }
        }, (err, roles) => {
            if (err)
                res.status(500).send("Error -> " + err);

            // update User with Roles
            savedUser.roles = roles.map(role => role._id);
            savedUser.save(function (err) {
                if (err)
                    res.status(500).send("Error -> " + err);

                res.status(200).send({
                    message: "User registered successfully!"
                });

            });
        });
    }).catch(err => {
        res.status(500).send("Fail! Error -> " + err);
    });
}

exports.signin = (req, res) => {
    console.log("Sign-In");
    var userRoles = [];
    User.findOne({
            username: req.body.username
        })
        .exec((err, user) => {

            if (err) {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with Username = " + req.body.username
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving User with Username = " + req.body.username
                });
            }
            var token = jwt.sign({
                id: user._id
            }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) {
                return res.status(401).send({
                    auth: false,
                    accessToken: null,
                    reason: "Invalid Password!"
                });
            }
            if (user.activation === "A") {
                if (user.roles.length == 1) {
                    Role.findOne({
                        _id: user.roles[0]
                    }, function (err, obj) {
                        userRoles[0] = obj.name;
                        console.log(userRoles);
                    }).then(data => {
                        res.status(200).send({
                            auth: true,
                            accessToken: token,
                            username: req.body.username,
                            authorities: userRoles
                        });
                    });
                } else {
                    res.status(200).send({
                        auth: true,
                        accessToken: token,
                        username: req.body.username,
                        authorities: ['ADMIN', 'USER']
                    });
                }
            } else {
                res.status(404).send({
                    message: "User has been deactivated = " + user.username
                });
            }
        });
}

exports.editAccount = (req, res) => {
    // Find user and update it
    User.findOneAndUpdate({
            username: req.body.username
        }, {
            username: req.body.newusername,
            password: bcrypt.hashSync(req.body.password, 8)
        }, {
            new: true
        })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "user not found " + req.body.username
                });
            }
            res.send("user edit successfully");
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "user not found " + req.body.username
                });
            }
            return res.status(500).send({
                message: "Error updating user  " + req.body.username
            });
        });
};



exports.deactivate = (req, res) => {
    User.findOne({
        username: req.body.username
    }).then(data => {
        User.findOneAndUpdate({
                username: req.body.username
            }, {
                username: data.username,
                activation: "D",
                password: bcrypt.hashSync(data.password, 8)
            }, {
                new: true
            })
            .then(user => {
                if (!user) {
                    return res.status(404).send({
                        message: "user not found  " + req.body.username
                    });
                }
                res.send(user);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "user not found  " + req.body.username
                    });
                }
                return res.status(500).send({
                    message: "Error updating user " + req.body.username
                });
            });
    })


};

exports.activate = (req, res) => {
    User.findOne({
        username: req.body.username
    }).then(data => {
        User.findOneAndUpdate({
                username: req.body.username
            }, {

                username: data.username,
                activation: "A",
                password: bcrypt.hashSync(data.password, 8)
            }, {
                new: true
            })
            .then(user => {
                if (!user) {
                    return res.status(404).send({
                        message: "user not found  " + req.body.username
                    });
                }
                res.send(user);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "user not found " + req.body.username
                    });
                }
                return res.status(500).send({
                    message: "Error updating user with id " + req.body.username
                });
            });
    })

}
exports.oneUser = (req, res) => {

    User.findOne({
            username: req.body.username
        })
        .exec((err, user) => {
            if (err) {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with Username = " + req.body.username
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving User with Username = " + req.body.username
                });
            }
            res.status(200).send(user);
        });

}

exports.createModule = (req, res) => {

    const moduleName = new ModuleName(req.body);

    moduleName.save().then(data => {
        res.json(data);
    }).catch(err => {
        res.status(500).json({
            msg: err.message
        });
    })

}

exports.getModule = (req, res) => {
    ModuleName.find()
        .then(module => {
            res.json(module);
        }).catch(err => {
            res.status(500).send({
                msg: err.message
            });
        });
};

exports.renameModule = (req, res) => {

   
    ModuleName.findOne({
        moduleName: req.body.moduleName
    }).then(data => {
        ModuleName.findOneAndUpdate({
                moduleName: req.body.moduleName
            }, {

                moduleName: req.body.moduleNewName,
            }, {
                new: true
            })
            .then(data => {
                if (!data) {
                    return res.status(404).send({
                        message: "Module Name not found  " + req.body.moduleName
                    });
                }
                res.send("Module Name Rename Successfully");
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "Module Name not found " + req.body.moduleName
                    });
                }
                return res.status(500).send({
                    message: "Error updating Module Name " + req.body.moduleName
                });
            });
    })

}

exports.deleteModule = (req, res) => {

    ModuleName.findOneAndDelete({
        moduleName: req.body.moduleName
    }).then(data => {
        res.json("Deleted Successfully"); 
    }).catch(err => {
        res.status(500).send({
            msg: err.message
        });
    });

}

exports.SubmitIssue = (req, res) => {

    const issue = new Issue(req.body);

    issue.save().then(data => {
        res.json(data);
    }).catch(err => {
        res.status(500).json({
            msg: err.message
        });
    })


}

exports.editIssue = (req, res) => {

    Issue.findOneAndUpdate({
        _id: req.body.id
    }, {
        title : req.body.title,
        status : req.body.status,
        defectSummary : req.body.defectSummary,
        defectDescription : req.body.defectDescription,
        actionTaken : req.body.actionTaken,
        referId : req.body.referId,
        defectId : req.body.defectId,
        moduleName : req.body.moduleName,
        verifiedBy : req.body.verifiedBy,
        ownerId : req.body.ownerId,
        assignedTo : req.body.assignedTo,
        priority : req.body.priority,
        severity : req.body.severity,
        verificationStatus : req.body.verificationStatus,
        defectInjected : req.body.defectInjected,
        defectCorrected : req.body.defectCorrected,
        defectDetected : req.body.defectDetected,
        solutionComment : req.body.solutionComment,
        defetctType : req.body.defetctType,
        department : req.body.department,
        requester : req.body.requester,
        plannedEffort : req.body.plannedEffort,
        assignedDate : req.body.assignedDate,
        expectedCloseDate : req.body.expectedCloseDate,
        actualEffort : req.body.actualEffort,
        actualFixedDate : req.body.actualFixedDate,
        testCaseId : req.body.testCaseId,
        defectPhaseId : req.body.defectPhaseId,
        resolutionTime : req.body.resolutionTime,
        responseTime : req.body.responseTime,
        slaResolutionTime : req.body.slaResolutionTime,
        slaResponseTime : req.body.slaResponseTime
    }, {
        new: true
    })
    .then(data => {
        if (!data) {
            return res.status(404).send({
                message: "Issue not found " + req.body.id
            });
        }
        res.send("issue edited successfully");
    }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "issue not found " + req.body.id
            });
        }
        return res.status(500).send({
            message: "Error updating issue  " + req.body.id
        });
    });

}


exports.getOneIssue = (req, res) => {

    Issue.findOne({
        _id: req.body.id
    })
    .exec((err, data) => {
        if (err) {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Issue not found with id = " + req.body.id
                });
            }
            return res.status(500).send({
                message: "Issue not found with id = " + req.body.id
            });
        }
        res.status(200).send(data);
    });

}

exports.deleteIssue = (req, res) => {
    console.log(req.body.id);
    Issue.deleteMany({ _id: { $in: req.body.id}}, function(err, data) {
        if (err)
       {
           res.send(err);
       }
       res.json("Issue Deleted Successfully");
    })
}


exports.getIssueByModule = (req, res) => {

    console.log(req.body.moduleName);

    Issue.find({moduleName: req.body.moduleName}, function(err, data) 
    {
       if (err)
       {
           res.send(err);
       }
       res.json(data);
    });   

}

exports.getIssueBySubCategory = (req, res) => {

    console.log( req.body.moduleName,req.body.category);

    Issue.find({moduleName: req.body.moduleName, defectId: req.body.category}, function(err, data) 
    {
       if (err)
       {
           res.send(err);
       }
     
       res.json(data);
   
    });
}

exports.getAllIssue = (req, res) => {

    Issue.find()
        .then(issue => {
            res.json(issue);
        }).catch(err => {
            res.status(500).send({
                msg: err.message
            });
        });

}