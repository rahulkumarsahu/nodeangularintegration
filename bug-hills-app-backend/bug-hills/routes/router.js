const verifySignUp = require('./verifySignUp');
module.exports = function(app) {

    const controller = require('../controller/controller');

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
        next();
      });
 
      app.post('/api/auth/signup', [verifySignUp.checkDuplicateUserNameOrEmail, verifySignUp.checkRolesExisted], controller.signup);
	
      app.post('/api/auth/signin', controller.signin);

      app.post('/api/auth/edit', controller.editAccount);

      app.post('/api/auth/deactivate', controller.deactivate);

      app.post('/api/auth/activate', controller.activate);

      app.post('/api/data/createModule', controller.createModule);

      app.post('/api/data/renameModule', controller.renameModule);

      app.post('/api/data/deleteModule', controller.deleteModule);

      app.get('/api/data/getModule', controller.getModule);

      app.post('/api/data/submitIssue', controller.SubmitIssue);

      app.post('/api/data/editIssue', controller.editIssue);

      app.post('/api/data/deleteIssue', controller.deleteIssue);

      app.post('/api/data/getIssueByName', controller.getIssueByModule);

      app.get('/api/data/getAllIssue', controller.getAllIssue);

      app.post('/api/data/getOneIssue', controller.getOneIssue);

      app.post('/api/data/getIssueBySubCategory', controller.getIssueBySubCategory)
}