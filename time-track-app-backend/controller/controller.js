const config = require('../config/config');
const csvjson = require('csvjson');
const Parser = require('json2csv');

const Role = require('../models/role.model');
const User = require('../models/user.model');
const TimeTask = require('../models/task.model');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');


exports.signup = (req, res) => {
    // Save User to Database
    console.log("Processing func -> SignUp");

    const user = new User({
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        phone: req.body.phone,
        activation: "A",
        password: bcrypt.hashSync(req.body.password, 8)
    });

    // Save a User to the MongoDB
    user.save().then(savedUser => {
        Role.find({
            'name': {
                $in: req.body.roles.map(role => role.toUpperCase())
            }
        }, (err, roles) => {
            if (err)
                res.status(500).send("Error -> " + err);

            // update User with Roles
            savedUser.roles = roles.map(role => role._id);
            savedUser.save(function (err) {
                if (err)
                    res.status(500).send("Error -> " + err);

                res.status(200).send({
                    message: "User registered successfully!"
                });

            });
        });
    }).catch(err => {
        res.status(500).send("Fail! Error -> " + err);
    });
}

exports.signin = (req, res) => {
    console.log("Sign-In");
    var userRoles = [];
    User.findOne({
            username: req.body.username
        })
        .exec((err, user) => {

            if (err) {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with Username = " + req.body.username
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving User with Username = " + req.body.username
                });
            }

            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) {
                return res.status(401).send({
                    auth: false,
                    accessToken: null,
                    reason: "Invalid Password!"
                });
            }

            var token = jwt.sign({
                id: user._id
            }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });

            if (user.activation === "A") {
                if (user.roles.length == 1) {
                    Role.findOne({
                        _id: user.roles[0]
                    }, function (err, obj) {
                        userRoles[0] = obj.name;
                        console.log(userRoles);
                    }).then(data => {
                        res.status(200).send({
                            auth: true,
                            accessToken: token,
                            username: req.body.username,
                            authorities: userRoles
                        });
                    });
                } else {
                    res.status(200).send({
                        auth: true,
                        accessToken: token,
                        username: req.body.username,
                        authorities: ['ADMIN', 'USER']
                    });
                }
            } else {
                res.status(404).send({
                    message: "User has been deactivated = " + user.username
                });
            }
        });
}

exports.editAccount = (req, res) => {
    // Find user and update it
    User.findOneAndUpdate({
            username: req.body.username
        }, {
            name: req.body.name,
            username: req.body.username,
            email: req.body.email,
            phone: req.body.phone,
            activation: "A",
            password: bcrypt.hashSync(req.body.password, 8)
        }, {
            new: true
        })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "user not found with id " + req.params.customerId
                });
            }
            res.send(user);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "user not found with id " + req.params.customerId
                });
            }
            return res.status(500).send({
                message: "Error updating user with id " + req.params.customerId
            });
        });
};



exports.deactivate = (req, res) => {
    User.findOne({
        username: req.body.username
    }).then(data => {
        User.findOneAndUpdate({
                username: req.body.username
            }, {
                name: data.name,
                username: data.username,
                email: data.email,
                phone: data.phone,
                activation: "D",
                password: bcrypt.hashSync(data.password, 8)
            }, {
                new: true
            })
            .then(user => {
                if (!user) {
                    return res.status(404).send({
                        message: "user not found with id " + req.params.customerId
                    });
                }
                res.send(user);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "user not found with id " + req.params.customerId
                    });
                }
                return res.status(500).send({
                    message: "Error updating user with id " + req.params.customerId
                });
            });
    })


};

exports.activate = (req, res) => {
    User.findOne({
        username: req.body.username
    }).then(data => {
        User.findOneAndUpdate({
                username: req.body.username
            }, {
                name: data.name,
                username: data.username,
                email: data.email,
                phone: data.phone,
                activation: "A",
                password: bcrypt.hashSync(data.password, 8)
            }, {
                new: true
            })
            .then(user => {
                if (!user) {
                    return res.status(404).send({
                        message: "user not found with id " + req.params.customerId
                    });
                }
                res.send(user);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "user not found with id " + req.params.customerId
                    });
                }
                return res.status(500).send({
                    message: "Error updating user with id " + req.params.customerId
                });
            });
    })

}
exports.oneUser = (req, res) => {

    User.findOne({
            username: req.body.username
        })
        .exec((err, user) => {
            if (err) {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with Username = " + req.body.username
                    });
                }
                return res.status(500).send({
                    message: "Error retrieving User with Username = " + req.body.username
                });
            }
            res.status(200).send(user);
        });

}

exports.taskSubmit = (req, res) => {
    console.log("Submiting Task");

    const task = new TimeTask({
        username: req.body.username,
        date: req.body.date,
        category: req.body.category,
        task: req.body.timeTask
    });

    task.save().then(data => {
        res.status(200).send({
            message: "data Submited successfully!",
            data
        });
    }).catch(err => {
        res.status(500).send("Fail! Error -> " + err);
    });

}

exports.getTask = (req, res) => {
    TimeTask.find(function (err, task) {
        if (err) throw err;
        const fields = ['username', 'date', 'category', 'task.task', 'task.hrs'];
        const json2csvParser = new Parser({
            fields,
            unwind: ['task']
        });
        var taskdata = JSON.stringify(task);
        var dataTask = JSON.parse(taskdata);
        const csv = json2csvParser.parse(dataTask);
        const jsonObj = csvjson.toObject(csv);
        res.status(200).send(jsonObj);
    })
}