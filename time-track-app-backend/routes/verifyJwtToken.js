const jwt = require('jsonwebtoken');
const config = require('../config/config');
const User = require('../models/user.model');
const Role = require('../models/role.model');

verifyToken = (req, res, next) => {
	let token = req.headers['x-access-token'];
  
	if (!token){
		return res.status(403).send({ 
			auth: false, message: 'No token provided.' 
		});
	}

	jwt.verify(token, config.secret, (err, decoded) => {
		if (err){
			return res.status(500).send({ 
					auth: false, 
					message: 'Fail to Authentication. Error -> ' + err 
				});
		}
		req.userId = decoded.id;
		next();
	});
}


const authJwt = {};
authJwt.verifyToken = verifyToken;


module.exports = authJwt;