const verifySignUp = require('./verifySignUp');
const authJwt = require('./verifyJwtToken');
module.exports = function(app) {

    const controller = require('../controller/controller');

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
        next();
      });
 
      app.post('/api/auth/signup', [verifySignUp.checkDuplicateUserNameOrEmail, verifySignUp.checkRolesExisted], controller.signup);
	
      app.post('/api/auth/signin', controller.signin);

      app.post('/api/auth/edit', controller.editAccount);

      app.post('/api/auth/deactivate', controller.deactivate);

      app.post('/api/auth/details', controller.oneUser);

      app.post('/api/auth/activate', controller.activate);

      app.post('/api/auth/task', controller.taskSubmit);

      app.get('/api/auth/get', controller.getTask);
}