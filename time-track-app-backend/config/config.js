module.exports = {
   'secret': 'customer-super-secret-key',
   url: 'mongodb://localhost:27017/time_track_db',
   ROLEs: ['USER', 'ADMIN']
};

