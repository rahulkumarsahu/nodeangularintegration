const mongoose = require('mongoose'), Schema = mongoose.Schema;
 
const TimeTaskSchema = mongoose.Schema({
		username: String,
		date: Date,
        category: String,
		task: []
});

module.exports = mongoose.model('TimeTask', TimeTaskSchema);