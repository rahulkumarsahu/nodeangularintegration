const Role = require('./role.model');
const mongoose = require('mongoose'), Schema = mongoose.Schema;
 
const UserSchema = mongoose.Schema({
		name: String,
		username: String,
		email: String,
        password: String,
        phone: String,
        activation: String,
		roles: [{ type: Schema.Types.ObjectId, ref: 'Role' }]
});

module.exports = mongoose.model('User', UserSchema);